<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aitech</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
</head>

<body>
    <div class="header">
        <div class="container">
            <div class="header-links">
                <nav>
                    <a href="index.php" class="logo">
                        <img src="images/logo2.png" alt="">
                    </a>
                    <li>
                        <a href="about.php">
                            О компании
                        </a>
                    </li>
                    <li>
                        <a href="products.php">
                            Продукция
                        </a>
                    </li>
                    <li>
                        <a href="solution.php">
                            Решения
                        </a>
                    </li>
                    <li>
                        <a href="partners.php">
                            Партнеры
                        </a>
                    </li>
                    <li>
                        <a href="contacts.php">
                            Контакты
                        </a>
                    </li>
                    <li>
                        <a href="letters.php">
                            Рекомендательные письма
                        </a>
                    </li>
                    <li>
                        <a href="vacancies.php">
                            Вакансии
                        </a>
                    </li>
                    <li>
                        <a href="production.php">
                            Cобственное производство
                        </a>
                    </li>
                    <select name="" id="">
                        <option value="">RU</option>
                        <option value="">KZ</option>
                    </select>
                </nav>
            </div>
        </div>
    </div>
        <div class="burger-menu-image">
            <a href="index.php">
                <img src="images/logo2.png" alt="">
            </a>
        </div>
        <div class="burger-menu-content">
            <a href="#" class="burger-menu-btn">
                <span class="burger-menu-lines"></span>
            </a>
        </div>
        <div class="nav-panel-mobil">
            <div class="container">
                <nav class="navbar-expand-lg navbar-light">
                    <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                        <li class="nav-item">
                            <a class="nav-link" href="about.php">О компании</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="production.php">Продукция</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="solution.php">Решения</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="partners.php">Партнеры</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contacts.php">Контакты</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="letters.php">Рекомендательные письма</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="vacancies.php">Вакансии</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="production.php">Собственное производство</a>
                        </li>
                        <select name="" id="">
                            <option value="">RU</option>
                            <option value="">KZ</option>
                        </select>
                    </ul>
                </nav>
            </div>
        </div>
