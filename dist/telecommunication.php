<?php include('header.php');?>
<div class="banner products-banner" style='background-image:url(images/bg-solution.png);'>
    <div class="container">
        <h1>Телекоммуникация</h1>
    </div>
</div>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item"><a href="#">Продукция</a></li>
                <li class="breadcrumb-item active" aria-current="page">Телекоммуникация</li>
            </ol>
        </nav>
    </div>
</div>
<div class="page-title">
    <div class="container">
        <h1>Телекоммуникация</h1>
    </div>
</div>
<div class="products-content">
    <div class="container">
        <h1>Совокупность электронного оборудования для предоставления услуг связи. С разделением по типу на пассивное и активное оборудование. 
            С разделением по принадлежности на операторские, клиентские, решения для бизнеса. По технологиям - проводное, беспроводное, спутниковое и оптическое. </h1>
        <p>Телекоммуникационные устройства необходимы для принятия информации от пользователя, обработки, передачи по различным средам, хранения информации, и доставки информации до адресата.</p>
        </div>
</div>


<?php include('footer.php');?>