<?php include('header.php');?>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Контакты</li>
            </ol>
        </nav>
    </div>
</div>
<div class="contacts">
    <div class="container">
        <div class="contact-top">
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="contacts-left">
                        <h1>Контакты</h1>
                        <div class="contacts-inner">
                            <div class="contacts-sub">
                                <img src="images/contact-map.png" alt="">
                                <p>A10A3P6, Республика Казахстан, г.Алматы,&nbsp; 
                                    Ауэзовский район, мкрн. №1, дом 68/3, офис №181</p>
                            </div>
                            <div class="contacts-sub">
                                <img src="images/contact-phone.png" alt="">
                                <a href="">+7 (727) 225 62 38</a>
                            </div>
                            <div class="contacts-sub contact-email">
                                <img src="images/contact-email.png" alt="">
                                <a href="">info@aitech.kz</a>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6 p-0">
                    <div class="contacts-right">
                        <iframe class="ggmap_contact" src="https://www.google.com/maps/embed?pb=!1m14!1m12!1m3!1d2055.8549200872994!2d76.86987089328292!3d43.22169693395642!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!5e0!3m2!1sru!2skz!4v1580210684145!5m2!1sru!2skz" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
                    </div>
                </div>
            </div>
        </div>
        <div class="contacts-bottom">
            <h1>Напишите нам, <br>  и мы свяжемся с вами</h1>
            <div class="row">
                <div class="col-xl-5 col-md-5">
                    <div class="contacts-left">
                        <input type="text" placeholder="Ваше имя* ">
                        <input type="text" placeholder="Ваш e-mail* ">
                        <button>Написать</button>
                    </div>
                </div>
                <div class="col-xl-7 col-md-7">
                    <div class="contacts-right">
                        <textarea name="" id="" cols="30" rows="10" placeholder="Ваше сообщение*"></textarea>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php');?>