<?php include('header.php');?>
<div class="banner " style='background-image:url(images/bg-video.png);'>
    <div class="container">
        <h1>Видеонаблюдение </h1>
    </div>
</div>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item"><a href="#">Продукция</a></li>
                <li class="breadcrumb-item active" aria-current="page">Видеонаблюдение </li>
            </ol>
        </nav>
    </div>
</div>
<div class="page-title">
    <div class="container">
        <h1>Видеонаблюдение </h1>
    </div>
</div>
<div class="products-content">
    <div class="container">
        <p>
            <b>Система видеонаблюдения – это комплекс охранных устройств, предназначенный для постоянного визуального
                наблюдения
                над защищаемой территорией.</b>
        </p>
        <p>Целью установки видеонаблюдения является создание предупредительных мер по защите территории, имущества,
            обеспечения безопасности граждан посредством ежеминутного наблюдения, как в режиме реального времени, так и
            в режиме последующего просмотра.</p>
        <p>Очень популярным в наши дни становится проект «Безопасный город». Во многих крупных городах установлены
            наружные камеры охранного видеонаблюдения прямо на улицах и информация с них поступает в единый центр
            безопасности. Непрерывная видеозапись (видеорегистрация) позволит в дальнейшем осуществить грамотный и
            безошибочный анализ.</p>
    </div>
</div>


<?php include('footer.php');?>