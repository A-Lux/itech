<?php include('header.php');?>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Рекомендательные письма</li>
            </ol>
        </nav>
    </div>
</div>
<div class="letters-page">
    <div class="container">
        <h1 class="title-info">Рекомендательные письма</h1>
        <div class="letters-slider">
            <div class="letters">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="letters">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="letters">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="letters">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="letters">
                <div class="row">
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                    <div class="col-xl-3 col-md-6 col-12">
                        <div class="letters-image">
                            <a href="images/letters.png" class="letters-zoom">
                                <img src="images/letters.png" alt="">
                            <div class="zoom-in-letter">
                                <img src="images/zoom-in-letters.png" alt="">
                                <p>Описание письма</p>
                            </div>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php');?>