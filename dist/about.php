<?php include('header.php');?>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">О компании</li>
            </ol>
        </nav>
    </div>
</div>
<div class="about">
    <div class="container">
        <div class="row">
            <div class="col-xl-6">
                <div class="about-slider-for">
                    <a href="images/about2.png" class='about-slide-image'>
                        <div class="about-slide-top">
                            <img src="images/about2.png" alt="">
                        </div>
                    </a>
                    <a href="images/about2.png" class='about-slide-image'>
                        <div class="about-slide-top">
                            <img src="images/about2.png" alt="">
                        </div>
                    </a>
                    <a href="images/about2.png" class='about-slide-image'>
                        <div class="about-slide-top">
                            <img src="images/about2.png" alt="">
                        </div>
                    </a>
                    <a href="images/about2.png" class='about-slide-image'>
                        <div class="about-slide-top">
                            <img src="images/about2.png" alt="">
                        </div>
                    </a>
                    <a href="images/about2.png" class='about-slide-image'>
                        <div class="about-slide-top">
                            <img src="images/about2.png" alt="">
                        </div>
                    </a>
                </div>
                <div class="about-slider-nav">
                    <div class="about-slide-bottom">
                        <img src="images/about.png" alt="">
                    </div>
                    <div class="about-slide-bottom">
                        <img src="images/about.png" alt="">
                    </div>
                    <div class="about-slide-bottom">
                        <img src="images/about.png" alt="">
                    </div>
                    <div class="about-slide-bottom">
                        <img src="images/about.png" alt="">
                    </div>
                    <div class="about-slide-bottom">
                        <img src="images/about.png" alt="">
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="about-content">
                    <h1>Собственное производство</h1>
                    <p>На протяжении нескольких лет наша компания не только подтверждает устойчивые позиции на рынке
                        поставок телекоммуникационного и оборудования для нефтепереработки,
                        но и успешно осваивает направления собственного производства.
                        С начала <b>2018</b> года Компания начала производство пластиковых изделий из ПВХ.
                    </p>
                </div>
            </div>
        </div>
        <div class="about-system-works">
            <h1>Как работает эта система?</h1>
            <div class="system-slider">
                <div class="container p-0">
                    <div class="system-slide">
                    <iframe   src="https://www.youtube.com/embed/hIEIGDsbKqY" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                        
                </div>
                </div>
                <div class="container p-0">
                    <div class="system-slide">
                    <iframe   src="https://www.youtube.com/embed/hIEIGDsbKqY" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                        
                </div>
                </div>
                <div class="container p-0">
                    <div class="system-slide">
                    <iframe   src="https://www.youtube.com/embed/hIEIGDsbKqY" frameborder="0"
                        allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                        allowfullscreen></iframe>
                        
                </div>
                </div>
                <div class="container p-0">
                    <div class="system-slide">
                        <iframe   src="https://www.youtube.com/embed/hIEIGDsbKqY" frameborder="0"
                            allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                           
                    </div>
                </div>
            </div>
        </div>
        
    </div>
</div>

<div class="partners partners-about">
    <div class="container">
        <h1 class="home-title">
            Партнеры
        </h1>
        <div class="partners-slider">
            <div class="partner">
                <img src="images/partners.png" alt="">
            </div>
            <div class="partner">
                <img src="images/partners2.png" alt="">
            </div>
            <div class="partner">
                <img src="images/partners3.png" alt="">
            </div>
            <div class="partner">
                <img src="images/partners4.png" alt="">
            </div>
            <div class="partner">
                <img src="images/partners5.png" alt="">
            </div>
            <div class="partner">
                <img src="images/partners6.png" alt="">
            </div>
            <div class="partner">
                <img src="images/partners7.png" alt="">
            </div>
            <div class="partner">
                <img src="images/partners.png" alt="">
            </div>
        </div>
    </div>
</div>


<?php include('footer.php');?>