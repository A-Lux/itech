<?php include('header.php');?>
<div class="banner call-center-banner">
    <div class="container">
        <h1>Call-центр </h1>
    </div>
</div>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item"><a href="#">Продукция</a></li>
                <li class="breadcrumb-item active" aria-current="page">Решения</li>
            </ol>
        </nav>
    </div>
</div>
<div class="page-title">
    <div class="container">
        <h1>Call-центр </h1>
    </div>
</div>
<div class="call-center">
    <div class="container">
        <p>Call-центр — система эффективной обратной связи с потребителем товаров и услуг либо поддержки, продвижения
            различных акций, социальных опросов, голосований. Является частью CRM-системы.</p>
        <div class="call-content">
            <h2>Задачи call-центров</h2>
            <div class="row">
                <div class="col-xl-6">
                    <div class="call-left">
                        <h1>К основным задачам call-центров относятся:</h1>
                        <ul>
                            <li>Правильность приема и обработки поступающей информации;</li>
                            <li>Оперативность реагирования на изменение в системе вызовов;</li>
                            <li>Создание, хранение и изменение баз данных по обратившимся клиентам;</li>
                            <li>Обновление программного и аппаратного обеспечения;</li>
                            <li>Постоянное обучение персонала;</li>
                            <li>Ведение статистики;</li>
                            <li>Маршрутизация вызовов по их специфике;</li>
                            <li>Максимальное использование автоматической системы интерактивного 
                                взаимодействия (IVR) для экономии времени;</li>
                            <li>Взаимодействие с другими отделами компании;</li>
                            <li>Улучшение обслуживания заказчика;</li>
                            <li>Уменьшение количества «необслуженных» вызовов.</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="call-right">
                        <img src="images/call-center-1.png" alt="">
                    </div>
                </div>
            </div>
        </div>
        <div class="call-content">
            <h2>Возможности call-центра</h2>
            <div class="row">
                <div class="col-xl-6">
                    <div class="call-content-bottom">
                        <ul>
                            <li>Регистрация звонков.</li>
                            <li>Хранение информации о клиенте с использованием аналитических подходов к истории звонков.</li>
                            <li>Создание, хранение и изменение баз данных по обратившимся клиентам;</li>
                            <li>Маршрутизация вызовов.</li>
                            <li>Запись разговоров.</li>
                            <li>Графическое (графики, схемы, диаграммы) отображение работы каждого оператора, отдела, всего центра</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="call-content-bottom">
                        <ul>
                            <li>Отображение на мониторе оператора информации о поступившем вызове.</li>
                            <li>Переадресации, создание очереди звонков, включение режима ожидания, 
                                автоматическое информирование о времени ожидания ответа.</li>
                            <li>Распределение звонков внутри группы операторов в зависимости от статуса.</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<?php include('footer.php');?>