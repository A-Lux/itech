<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Aitech</title>
    <link rel="stylesheet" href="css/main.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
</head>

<body>
    <div class="header-home">
        <div class="container">
            <div class="header-links">
                <nav>
                    <a href="index.php" class="logo">
                        <img src="images/logo-home.png" alt="">
                    </a>
                    <li>
                        <a href="about.php">
                            О компании
                        </a>
                    </li>
                    <li>
                        <a href="production.php">
                            Продукция
                        </a>
                    </li>
                    <li>
                        <a href="solution.php">
                            Решения
                        </a>
                    </li>
                    <li>
                        <a href="partners.php">
                            Партнеры
                        </a>
                    </li>
                    <li>
                        <a href="contacts.php">
                            Контакты
                        </a>
                    </li>
                    <li>
                        <a href="letters.php">
                            Рекомендательные письма
                        </a>
                    </li>
                    <li>
                        <a href="vacancies.php">
                            Вакансии
                        </a>
                    </li>
                    <li>
                        <a href="production.php">
                            Cобственное производство
                        </a>
                    </li>
                    <select name="" id="">
                        <option value="">RU</option>
                        <option value="">KZ</option>
                    </select>
                </nav>
            </div>
        </div>
    </div>
        <div class="burger-menu-image">
            <a href="index.php">
                <img src="images/logo-home.png" alt="">
            </a>
        </div>
        <div class="burger-menu-content">
            <a href="#" class="burger-menu-btn">
                <span class="burger-menu-lines"></span>
            </a>
        </div>
        <div class="nav-panel-mobil">
            <div class="container">
                <nav class="navbar-expand-lg navbar-light">
                    <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                        <li class="nav-item">
                            <a class="nav-link" href="about.php">О компании</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="production.php">Продукция</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="solution.php">Решения</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="partners.php">Партнеры</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="contacts.php">Контакты</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="letters.php">Рекомендательные письма</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="vacancies.php">Вакансии</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" href="production.php">Собственное производство</a>
                        </li>
                        <select name="" id="">
                            <option value="">RU</option>
                            <option value="">KZ</option>
                        </select>
                    </ul>
                </nav>
            </div>
        </div>
    <div class="home-banner">
        <div class="banner-sliders">
            <div class="banner-slide">
                <div class="banner-content">
                    <div class="banner-logo">
                        <img src="images/logo-main.png" alt="">
                    </div>
                    <div class="banner-text">
                        <p>Мы видим свою миссию в том, что расширяем возможности развития отечественных информационных и
                            телекоммуникационных систем, путем тщательного подбора и поставок
                            высококачественного оборудования, отвечающего всем высоким требованиям.</p>
                    </div>
                </div>
            </div>
            <div class="banner-slide">
                <div class="banner-content">
                    <div class="banner-logo">
                        <img src="images/logo-main.png" alt="">
                    </div>
                    <div class="banner-text">
                        <p>Мы видим свою миссию в том, что расширяем возможности развития отечественных информационныхи
                            телекоммуникационных систем, путем тщательного подбора и поставок
                            высококачественногооборудования, отвечающего всем высоким требованиям.</p>
                    </div>
                </div>
            </div>
            <div class="banner-slide">
                <div class="banner-content">
                    <div class="banner-logo">
                        <img src="images/logo-main.png" alt="">
                    </div>
                    <div class="banner-text">
                        <p>Мы видим свою миссию в том, что расширяем возможности развития отечественных информационныхи
                            телекоммуникационных систем, путем тщательного подбора и поставок
                            высококачественногооборудования, отвечающего всем высоким требованиям.</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="about-company">
        <div class="container">
            <div class="about-content">
                <h1 class="home-title">О компании</h1>
                <p> <span>Товарищество с ограниченной ответственностью «Ai-Tech»</span> - молодая, но стремительно
                    развивающаяся
                    компания, занявшая свое востребованное место на рынке интеграции информационных систем, которая
                    <span> была
                        основана в 2016 году. </span></p>
                <a href="about.php" class='more-details'>Подробнее</a>
            </div>
        </div>
    </div>
    <div class="advantages">
        <div class="container">
            <h1 class="home-title">Наши преимущества</h1>
            <div class="advantages-inner row">
                <div class="col-xl-3 p-0 col-md-6">
                    <div class="advantages-content">
                        <div class="advantage-image advantage-image-1">
                            <img src="" alt="">
                        </div>
                        <div class="advantage-text">
                            <p>Кратчайшие сроки поставок </p>
                        </div>
                    </div>
                </div>
               <div class="col-xl-3 p-0 col-md-6">
                <div class="advantages-content">
                    <div class="advantage-image advantage-image-2">
                        <img src="" alt="">
                    </div>
                    <div class="advantage-text">
                        <p>Опытные специалисты</p>
                    </div>
                </div>
               </div>
               <div class="col-xl-3 p-0 col-md-6">
                <div class="advantages-content">
                    <div class="advantage-image advantage-image-3">
                        <img src="" alt="">
                    </div>
                    <div class="advantage-text">
                        <p>Низкие <br> цены</p>
                    </div>
                </div>
               </div>
                <div class="col-xl-3 p-0 col-md-6">
                    <div class="advantages-content">
                        <div class="advantage-image advantage-image-4">
                            <img src="" alt="">
                        </div>
                        <div class="advantage-text">
                            <p>Выгодные решения для заказчиков </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="solutions">
        <div class="container">
            <h1 class="home-title">
                Решения
            </h1>
            <div class="solution-info">
                <div class="solutions-slider">
                    <div class="solution-slide">
                        <div class="solution-image">
                            <a href="images/solutions-image.png" class="solution-image">
                                <img src="images/solutions-image.png" alt="">
                                <span><img src="images/magnifier.png" alt=""></span>
                            </a>
                        </div>
                        <div class="solution-content">
                            <h1>Система «Умный дом»</h1>
                            <p>Новый Veeam® Backup & Replication™ 9.5 помогает обеспечить сверхвысокую доступность
                                данных
                                для ВСЕХ приложений виртуальной, физической и облачной среды. С помощью единой консоли
                                вы
                                можете управлять быстрым, гибким и надежным резервным копированием.</p>
                            <div class="solution-btn">
                                <a href="smart-home.php" class='more-details'>Подробнее</a>
                            </div>
                        </div>

                    </div>
                    <div class="solution-slide">
                        <div class="solution-image">
                            <a href="images/solutions-image.png" class="solution-image">
                                <img src="images/solutions-image.png" alt="">
                                <span><img src="images/magnifier.png" alt=""></span>
                            </a>
                        </div>
                        <div class="solution-content">
                            <h1>Система «Умный дом»</h1>
                            <p>Новый Veeam® Backup & Replication™ 9.5 помогает обеспечить сверхвысокую доступность
                                данных
                                для ВСЕХ приложений виртуальной, физической и облачной среды. С помощью единой консоли
                                вы
                                можете управлять быстрым, гибким и надежным резервным копированием.</p>
                            <div class="solution-btn">
                                <a href="smart-home.php" class='more-details'>Подробнее</a>
                            </div>
                        </div>
                    </div>
                    <div class="solution-slide">
                        <div class="solution-image">
                            <a href="images/solutions-image.png" class="solution-image">
                                <img src="images/solutions-image.png" alt="">
                                <span><img src="images/magnifier.png" alt=""></span>
                            </a>
                        </div>
                        <div class="solution-content">
                            <h1>Система «Умный дом»</h1>
                            <p>Новый Veeam® Backup & Replication™ 9.5 помогает обеспечить сверхвысокую доступность
                                данных
                                для ВСЕХ приложений виртуальной, физической и облачной среды. С помощью единой консоли
                                вы
                                можете управлять быстрым, гибким и надежным резервным копированием.</p>
                            <div class="solution-btn">
                                <a href="smart-home.php" class='more-details'>Подробнее</a>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="solutions-link">
                    <a href="">Смотреть все</a>
                </div>
            </div>
        </div>
    </div>
    <h1 class="home-title">
        Сертификаты и лицензии
    </h1>
    <div class="certificates">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-12 col-md-6">
                    <div class="certificates-text certificates-left">
                        <h1>СИСТЕМЫ МЕНЕДЖМЕНТА</h1>
                        <p>в начале 2018 года Товарищество
                            с ограниченной ответственностью «Ai-Tech»
                            прошло сертификацию по: </p>
                        <ul>
                            <li>системе менеджмента качества СТ РК ISO 9001-2016 (ISO 9001:2015)</li>
                            <li>системе экологического менеджмента СТ РК ISO 14001-2016 (ISO 14001:2015) </li>
                            <li>системе менеджмента профессиональной безопасности</li>
                        </ul>
                    </div>
                </div>
                <div class="col-xl-6 col-12 col-md-6">
                    <div class="certificates-text">
                        <h1>ЛИЦЕНЗИИ</h1>
                        <ul>
                            <li>Государственная лицензия на осуществление
                                деятельности по разработке и реализации
                                средств криптографической защиты информации</li>
                            <li>Сертификат о происхождении товара форма СТ-KZ</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="certificate-bottom">
        <div class="container">
            <div class="certificates-slider">
                <div class="certificate-slide">
                    <a href="images/certificate.png" class="certificate-image">
                        <img src="images/certificate.png" alt="">
                        <span><img src="images/magnifier2.png" alt=""></span>
                    </a>
                </div>
                <div class="certificate-slide">
                    <a href="images/certificate.png" class="certificate-image">
                        <img src="images/certificate.png" alt="">
                        <span><img src="images/magnifier2.png" alt=""></span>
                    </a>
                </div>
                <div class="certificate-slide">
                    <a href="images/certificate.png" class="certificate-image">
                        <img src="images/certificate.png" alt="">
                        <span><img src="images/magnifier2.png" alt=""></span>
                    </a>
                </div>
                <div class="certificate-slide">
                    <a href="images/certificate.png" class="certificate-image">
                        <img src="images/certificate.png" alt="">
                        <span><img src="images/magnifier2.png" alt=""></span>
                    </a>
                </div>
                <div class="certificate-slide">
                    <a href="images/certificate.png" class="certificate-image">
                        <img src="images/certificate.png" alt="">
                        <span><img src="images/magnifier2.png" alt=""></span>
                    </a>
                </div>
            </div>
        </div>
    </div>
    <div class="advantages-bottom">
        <div class="container">
            <h1 class="home-title">
                Преимущества работы <br> с нашей компанией
            </h1>
            <div class="row">
                <div class="col-xl-6 col-md-6">
                    <div class="advantage-list">
                        <div class="advantage-list-image">
                            <img src="images/adv1.png" alt="">
                        </div>
                        <div class="advantage-list-text">
                            <h1>Кратчайшие <br> сроки поставок </h1>
                            <p>Наша компания гарантирует, что в независимости
                                от бюджета и габаритов, продукция будет доставлена
                                в максимально короткие сроки.</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="advantage-list">
                        <div class="advantage-list-image">
                            <img src="images/adv2.png" alt="">
                        </div>
                        <div class="advantage-list-text">
                            <h1>Опытные специалисты </h1>
                            <p>В компании работают специалисты имеющие конкретный опыт в реализации сложных и интересных
                                проектов
                                в области телекоммуникации в Республике Казахстан
                            </p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="advantage-list">
                        <div class="advantage-list-image">
                            <img src="images/adv3.png" alt="">
                        </div>
                        <div class="advantage-list-text">
                            <h1>Низкие цены </h1>
                            <p>Высокое качество импортируемых
                                и экспортируемых поставок, соответствующих
                                параметрам «цена-качество»</p>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6 col-md-6">
                    <div class="advantage-list">
                        <div class="advantage-list-image">
                            <img src="images/adv4.png" alt="">
                        </div>
                        <div class="advantage-list-text">
                            <h1>Выгодные решения <br> для заказчиков </h1>
                            <p>Результатом высокого спроса заказчиков на рынке телекоммуникации,
                                послужило создание проектов
                                под ключ, обеспечение полной интеграции, спрос
                                на которых растет с каждым днем
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="key-areas">
        <div class="container">
            <h1 class="home-title">Ключевые направления</h1>
            <div class="row">
                <div class="col-xl-6">
                    <div class="key-areas-text">
                        <h1>Поставка оборудования и комплектующими
                            материалами на рынке телекоммуникации</h1>
                        <p>Ключевым видом деятельности компании является – оперативная доставка и обеспечение
                            оборудованием и комплектующими материалами на отечественном телекоммуникационном рынке.
                            Ярким примером, которого служат налажено отработанные проекты
                            по поставке телекоммуникационного оборудования для
                            <span>АО «Казахтелеком»</span>, наряду со многими его региональными филиалами, <span>АО «Эйр
                                Астана», АО "Казпочта», АО "КТЖ-Грузовые перевозки»
                                АО "Интергаз Центральная Азия" ,ТОО "СП "Казгермунай», ТОО "Байланыс НАК», АО "Шалкия
                                Цинк ЛТД"</span> </p>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="key-areas-image">
                        <img src="images/key-areas.png" alt="">
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="key-areas-image">
                        <img src="images/key-areas2.png" alt="">
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="key-areas-text key-areas-bottom">
                        <h1>Поставка оборудования и комплектующими материалами для нефтедобывающих </h1>
                        <p>Не менее успешно реализованы и так же находятся на стадии реализации проекты по поставке
                            оборудования
                            для нефтедобывающих компании, таких как <span> АО «Матен Петролеум», АО «КоЖаН», АО
                                «Озенмунайгаз»</span> </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="contacts">
        <div class="container">
            <div class="contacts-info">
                <div class="row">
                    <div class="col-xl-6">
                        <div class="contacts-left">
                            <h1>Контакты</h1>
                            <div class="contacts-content">
                                <img src="images/map.png" alt="">
                                <p>A10A3P6, Республика Казахстан,
                                    г.Алматы,  Ауэзовский район,
                                    мкрн. №1, дом 68/3, офис №181</p>
                            </div>
                            <div class="contacts-content">
                                <img src="images/phone.png" alt="">
                                <p>+7 (727) 225 62 38</p>
                            </div>
                            <div class="contacts-content contacts-email">
                                <img src="images/email.png" alt="">
                                <p>info@aitech.kz</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="contacts-right">
                            <textarea type="text" placeholder="Ваше сообщение "></textarea>
                            <input type="text" placeholder="Номер телефона*" class="contact-input">
                            <input type="text" placeholder="Ваше имя*" class="contact-input">
                            <input type="text" placeholder="Ваш e-mail*" class="contact-input">
                            <label class="label--checkbox">
                                <input type="checkbox" class="checkbox" checked>
                                Даю согласие на обработку своих данных
                            </label>
                            <a href="" class="more-details">Написать</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="partners">
        <div class="container">
            <h1 class="home-title">
                Партнеры
            </h1>
            <div class="partners-slider">
                <div class="partner">
                    <img src="images/partners.png" alt="">
                </div>
                <div class="partner">
                    <img src="images/partners2.png" alt="">
                </div>
                <div class="partner">
                    <img src="images/partners3.png" alt="">
                </div>
                <div class="partner">
                    <img src="images/partners4.png" alt="">
                </div>
                <div class="partner">
                    <img src="images/partners5.png" alt="">
                </div>
                <div class="partner">
                    <img src="images/partners6.png" alt="">
                </div>
                <div class="partner">
                    <img src="images/partners7.png" alt="">
                </div>
                <div class="partner">
                    <img src="images/partners.png" alt="">
                </div>
            </div>
        </div>
    </div>
    <?php include('footer.php');?>