<?php include('header.php');?>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Партнеры</li>
            </ol>
        </nav>
    </div>
</div>
<div class="partners-page ">
    <div class="container">
        <h1 class="title-info">Партнеры</h1>
        <p>Рады сообщить, что долгосрочными усилиями профессиональных сотрудников нашей компании налажена работа по
            импорту с зарубежными поставщиками, включая дистрибьюторов, поэтому наши цены всегда конкурентоспособны и
            возможны к обсуждению, в случае присутствия конкурентных предложений. Также при работе с постоянными
            клиентами мы увеличиваем наличие на нашем складе товаров, которые периодически закупаются данными клиентами,
            т.е. мы готовы держать под Вас на нашем складе определенные товары для самой быстрой их поставки в случае
            необходимости. За время работы, заказчикам были поставлены оборудование и комплектующие от мировых
            производителей, таких как:</p>
            <div class="partners">
                <div class="container">
                    <div class="partners-slider">
                        <div class="partner">
                            <img src="images/partners.png" alt="">
                        </div>
                        <div class="partner">
                            <img src="images/partners2.png" alt="">
                        </div>
                        <div class="partner">
                            <img src="images/partners3.png" alt="">
                        </div>
                        <div class="partner">
                            <img src="images/partners4.png" alt="">
                        </div>
                        <div class="partner">
                            <img src="images/partners5.png" alt="">
                        </div>
                        <div class="partner">
                            <img src="images/partners6.png" alt="">
                        </div>
                        <div class="partner">
                            <img src="images/partners7.png" alt="">
                        </div>
                        <div class="partner">
                            <img src="images/partners.png" alt="">
                        </div>
                    </div>
                </div>
            </div>
    </div>
</div>


<?php include('footer.php');?>