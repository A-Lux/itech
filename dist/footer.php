<div class="footer">
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-12 col-md-6">
                <div class="footer-link">
                    <a href="home.php"><img src="images/logo-home.png" alt=""></a>
                    <p class="copyright">© 2016-2020 <br> Все права защищены </p>
                    <div class="social-links">
                        <a href=""><img src="images/youtube.png" alt=""></a>
                        <a href=""><img src="images/instagram.png" alt=""></a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-12 col-md-6">
                <div class="footer-link footer-link2">
                    <div class="footer-sub">
                        <a href="about.php">О компании</a>
                        <a href="products.php"> Продукция</a>
                    </div>
                    <div class="footer-sub">
                        <a href="solution.php">Решения</a>
                        <a href="partners.php"> Партнеры</a>
                    </div>
                    <div class="footer-sub">
                        <a href="contacts.php">Контакты</a>
                        <a href="vacancies.php">Вакансии</a>
                    </div>
                    <div class="footer-sub">
                        <a href="letters.php">Рекомендательные письма</a>
                    </div>
                    <div class="footer-sub">
                        <a href="production.php">Собственное производство</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-12 col-md-6">
                <div class="footer-link">
                    <div class="footer-sub">
                        <img src="images/footer-map.png" alt="">
                        <p>A10A3P6, Республика Казахстан, г.Алматы,  
                            Ауэзовский район, мкрн. №1, дом 68/3, офис №181</p>
                    </div>
                    <div class="footer-sub">
                        <img src="images/footer-phone.png" alt="">
                        <a href="">+7 (727) 225 62 38</a>
                    </div>
                    <div class="footer-sub footer-email">
                        <img src="images/footer-email.png" alt="">
                        <a href="">info@aitech.kz</a>
                    </div>
                </div>
            </div>
            <div class="col-xl-3 col-12 col-md-6">
                <div id="map" style="width: 306px; height: 204px">

                </div>
            </div>
        </div>
    </div>
</div>
<script src="https://api-maps.yandex.ru/2.1/?&lang=ru_RU" type="text/javascript"></script>
<script src="js/main.js"></script>
</body>

</html>