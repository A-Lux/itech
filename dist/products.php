<?php include('header.php');?>
<div class="banner products-banner">
    <div class="container">
        <h1>Продукция </h1>
    </div>
</div>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Продукция</li>
            </ol>
        </nav>
    </div>
</div>
<div class="products">
    <div class="container">
        <div class="row">
            <div class="col-xl-4 col-md-6">
                <div class="products-info">
                    <h1>Оборудование беспроводной связи </h1>
                    <div class="products-content" style="background-image: url(images/products1.png);background-size: cover;">
                        <div class="products-text">
                            <p>Построение корпоративных, публичных и операторских беспроводных сетей</p>
                            <a href="">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="products-info">
                    <h1>Оборудование оптической связи </h1>
                    <div class="products-content" style="background-image: url(images/products2.png);background-size: cover;">
                        <div class="products-text">
                            <p>Технология GPON - новый стандарт скоростей</p>
                            <a href="">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="products-info">
                    <h1>Оборудование проводной связи</h1>
                    <div class="products-content" style="background-image: url(images/products3.png);background-size: cover;">
                        <div class="products-text">
                            <p>Построение операторских сетей: xDSL, ETTx, NGN, Metro Ethernet, Магистральные сети</p>
                            <a href="">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="products-info">
                    <h1>Серверное оборудование</h1>
                    <div class="products-content" style="background-image: url(images/products4.png);background-size: cover;">
                        <div class="products-text">
                            <p>Решения для хранения и управления информацией. ЦОДы, СХД, сервера.</p>
                            <a href="">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="products-info">
                    <h1>Оборудование IP-телефонии</h1>
                    <div class="products-content" style="background-image: url(images/call-center-1.png);background-size: cover;">
                        <div class="products-text">
                            <p>Новый подход к развитию собственной инфраструктуры</p>
                            <a href="call-center.php">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="products-info">
                    <h1>Видеонаблюдение</h1>
                    <div class="products-content" style="background-image: url(images/video.png);background-size: cover;">
                        <div class="products-text">
                            <p>Видеоконференцсвязь (ВКС) и телеприсутствие, решения Cisco TelePresence.</p>
                            <a href="video-monitoring.php">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="products-info">
                    <h1>Умный дом</h1>
                    <div class="products-content" style="background-image: url(images/smart-home.png);background-size: cover;">
                        <div class="products-text">
                            <p>Новый подход к развитию собственной инфраструктуры</p>
                            <a href="smart-home.php">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="products-info">
                    <h1>Освещение</h1>
                    <div class="products-content" style="background-image: url(images/light.png);background-size: cover;">
                        <div class="products-text">
                            <p>Безопасность бизнеса – безопасность Ваших финансов</p>
                            <a href="lighting.php">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-md-6">
                <div class="products-info products-info-6">
                    <h1>Оборудование для гарантированного питания</h1>
                    <div class="products-content" style="background-image: url(images/products6.png);background-size: cover;">
                        <div class="products-text">
                            <p>Решения информационной безопасности</p>
                            <a href="">Ознакомиться</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<?php include('footer.php');?>