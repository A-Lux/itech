<?php include('header.php');?>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Вакансии</li>
            </ol>
        </nav>
    </div>
</div>
<div class="vacancies-page">
    <div class="container">
        <h1 class="title-info">Вакансии</h1>
        <div class="vacancy-content">
            <div class="row">
                <div class="col-xl-5 col-12 col-md-5">
                    <div class="vacancy-image">
                        <img src="images/vacancy.png" alt="">
                    </div>
                </div>
                <div class="col-xl-7 col-12 col-md-7">
                    <div class="vacancy-info">
                        <h1>Менеджер отдела</h1>
                        <span>14.10.2018</span>
                        <p>Заработная плата: <b> 150 000 тг</b></p>
                        <p>Требуемый опыт: <b>1 год</b></p>
                        <div class="duties-content">
                            <div class="duties-left">
                                <p>Обязанности:</p>
                            </div>
                            <div class="duties-right">
                                <ul>
                                    <li>
                                        Вести клиентскую базу и выстраивать отношения с клиентами;</li>
                                    <li>Привлекать новых клиентов. Исходящие звонки по базе, обработка входящих звонков;
                                    </li>
                                    <li>Работа в CRM системе;</li>
                                    <li>Выполнение плана продаж;</li>
                                    <li> Презентация продуктов компании;</li>
                                    <li>Составление коммерческих предложений;</li>
                                </ul>
                            </div>
                        </div>
                        <p>Резюме высылать на почту <b>cv@aitech.kz</b></p>
                    </div>
                </div>
            </div>
        </div>
        <div class="vacancy-content">
            <div class="row">
                <div class="col-xl-5 col-12 col-md-5">
                    <div class="vacancy-image">
                        <img src="images/vacancy.png" alt="">
                    </div>
                </div>
                <div class="col-xl-7 col-12 col-md-7">
                    <div class="vacancy-info">
                        <h1>Менеджер отдела</h1>
                        <span>14.10.2018</span>
                        <p>Заработная плата: <b> 150 000 тг</b></p>
                        <p>Требуемый опыт: <b>1 год</b></p>
                        <div class="duties-content">
                            <div class="duties-left">
                                <p>Обязанности:</p>
                            </div>
                            <div class="duties-right">
                                <ul>
                                    <li>
                                        Вести клиентскую базу и выстраивать отношения с клиентами;</li>
                                    <li>Привлекать новых клиентов. Исходящие звонки по базе, обработка входящих звонков;
                                    </li>
                                    <li>Работа в CRM системе;</li>
                                    <li>Выполнение плана продаж;</li>
                                    <li> Презентация продуктов компании;</li>
                                    <li>Составление коммерческих предложений;</li>
                                </ul>
                            </div>
                        </div>
                        <p>Резюме высылать на почту <b>cv@aitech.kz</b></p>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <?php include('footer.php');?>