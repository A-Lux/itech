<?php include('header.php');?>
<div class="banner solution-banner">
    <div class="container">
        <h1>Решения</h1>
    </div>
</div>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item active" aria-current="page">Решения</li>
            </ol>
        </nav>
    </div>
</div>
<div class="solution">
    <div class="container">
        <div class="solution-inner">
            <div class="solution-image">
                <img src="images/telecom1.png" alt="">
            </div>
            <div class="solution-content">
                <h1>Телекоммуникация</h1>
                <p>Новый Veeam® Backup & Replication™ 9.5 помогает обеспечить сверхвысокую доступность данных для ВСЕХ
                    приложений виртуальной, физической и облачной среды. С помощью единой консоли вы можете управлять
                    быстрым, гибким и надежным резервным копированием.</p>
                <span>Масштаб: <p>проект городского/районного значения</p></span>
                <span>Техническая сложность: <p>высокая</p></span>
                <a href="telecommunication.php">Читать подробнее...</a>
            </div>
        </div>
        <div class="solution-inner">
            <div class="solution-image">
                <img src="images/light.png" alt="">
            </div>
            <div class="solution-content">
                <h1>Освещение</h1>
                <p>Новый Veeam® Backup & Replication™ 9.5 помогает обеспечить сверхвысокую доступность данных для ВСЕХ
                    приложений виртуальной, физической и облачной среды. С помощью единой консоли вы можете управлять
                    быстрым, гибким и надежным резервным копированием.</p>
                <span>Масштаб: <p>проект городского/районного значения</p></span>
                <span>Техническая сложность: <p>высокая</p></span>
                <a href="lighting.php">Читать подробнее...</a>
            </div>
        </div>
        <div class="solution-inner">
            <div class="solution-image">
                <img src="images/video.png" alt="">
            </div>
            <div class="solution-content">
                <h1>Видеонаблюдение</h1>
                <p>Новый Veeam® Backup & Replication™ 9.5 помогает обеспечить сверхвысокую доступность данных для ВСЕХ
                    приложений виртуальной, физической и облачной среды. С помощью единой консоли вы можете управлять
                    быстрым, гибким и надежным резервным копированием.</p>
                <span>Масштаб: <p>проект городского/районного значения</p></span>
                <span>Техническая сложность: <p>высокая</p></span>
                <a href="video-monitoring.php">Читать подробнее...</a>
            </div>
        </div>
        <div class="solution-inner">
            <div class="solution-image">
                <img src="images/smart-home.png" alt="">
            </div>
            <div class="solution-content">
                <h1>Система «Умный дом»</h1>
                <p>Новый Veeam® Backup & Replication™ 9.5 помогает обеспечить сверхвысокую доступность данных для ВСЕХ
                    приложений виртуальной, физической и облачной среды. С помощью единой консоли вы можете управлять
                    быстрым, гибким и надежным резервным копированием.</p>
                <span>Масштаб: <p>проект городского/районного значения</p></span>
                <span>Техническая сложность: <p>высокая</p></span>
                <a href="smart-home.php">Читать подробнее...</a>
            </div>
        </div>
        <div class="solution-inner solution-bottom">
            <div class="solution-image">
                <img src="images/call-center.png" alt="">
            </div>
            <div class="solution-content">
                <h1>CALL - центр</h1>
                <p>Новый Veeam® Backup & Replication™ 9.5 помогает обеспечить сверхвысокую доступность данных для ВСЕХ
                    приложений виртуальной, физической и облачной среды. С помощью единой консоли вы можете управлять
                    быстрым, гибким и надежным резервным копированием.</p>
                <span>Масштаб: <p>проект городского/районного значения</p></span>
                <span>Техническая сложность: <p>высокая</p></span>
                <a href="call-center.php">Читать подробнее...</a>
            </div>
        </div>
    </div>
</div>
<?php include('footer.php');?>