<?php include('header.php');?>
<div class="banner" style='background-image:url(images/bg-smart-home.png);'>
    <div class="container">
        <h1>Система “Умный дом”</h1>
    </div>
</div>
<div class="breadcrumbs">
    <div class="container">
        <nav aria-label="breadcrumb">
            <ol class="breadcrumb">
                <li class="breadcrumb-item"><a href="#">Главная</a></li>
                <li class="breadcrumb-item"><a href="#">Продукция</a></li>
                <li class="breadcrumb-item active" aria-current="page">Система “Умный дом”</li>
            </ol>
        </nav>
    </div>
</div>
<div class="page-title">
    <div class="container">
        <h1>Система умный дом</h1>
    </div>
</div>
<div class="smart-home">
    <div class="container">
        <p>Данная система позволяет не только экономить время человека, но и обеспечивает надежность и безопасность дома
            и квартиры. Система Умный Дом - это мозговой центр любого дома, предприятия, офиса и квартиры. Автоматизация
            дома создает неповторимый комфорт и уют, не говоря уже о повышенной безопасности.</p>
        <div class="smart-home-content">
            <h1>Система Smart Home включает в себя три основных элемента:</h1>
            <div class="row">
                <div class="col-xl-6">
                    <div class="smart-home-left">
                        <div class="smart-wrapper">
                            <div class="smart-icons">
                                <img src="images/smart1.png" alt="">
                            </div>
                            <div class="smart-text">
                                <p>Датчики, воспринимающие сигналы  и информацию из окружающей среды</p>
                            </div>
                        </div>
                        <div class="smart-wrapper">
                            <div class="smart-icons">
                                <img src="images/smart2.png" alt="">
                            </div>
                            <div class="smart-text">
                                <p>Центральный процессор (хаб), обрабатывающий эти сигналы и принимающий решения;</p>
                            </div>
                        </div>
                        <div class="smart-wrapper">
                            <div class="smart-icons">
                                <img src="images/smart3.png" alt="">
                            </div>
                            <div class="smart-text">
                                <p>Устройства-исполнители (актуаторы), которые получают указания от хаба и непосредственно выполняют задачи по дому.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="smart-home-right">
                        <img src="images/smart4.png" alt="">
                    </div>
                </div>
            </div>
            <h1>Возможности нашей системы</h1>
            <p>Современные системы «умного дома» допускают наличие самых разных элементов. Это могут быть умные розетки, системы видеонаблюдения, управляемые термостаты, системы климат контроля, умные дверные замки, системы сигнализации, робот-пылесос и пр.</p>
        </div>
    </div>
</div>

<?php include('footer.php');?>