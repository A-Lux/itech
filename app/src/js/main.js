window.$ = window.jQuery = require("jquery");
require('bootstrap');
require('slick-carousel');
require('magnific-popup');
require('swiper');
import {
  Swiper,
  Navigation,
  Pagination,
  Scrollbar,
  EffectCoverflow
} from 'swiper/js/swiper.esm.js';
Swiper.use([Navigation, Pagination, Scrollbar, EffectCoverflow]);
$(".banner-sliders").slick({
  slidesToShow: 1,
  infinite: true,
  nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
  prevArrow: '<img src="../images/arrow-left.png" alt="" class="slick-prev-btn">',
})



$(".solutions-slider").slick({
  slidesToShow: 1,
  infinite: true,
  nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
  prevArrow: '<img src="../images/arrow-left.png" alt="" class="slick-prev-btn">',
  responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        // dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
})

$(".certificates-slider").slick({
  slidesToShow: 4,
  infinite: true,
  nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
  prevArrow: '<img src="../images/arrow-left.png" alt="" class="slick-prev-btn">',
  responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1,
        infinite: true,
        // dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
})

$(".letters-slider").slick({
  slidesToShow: 1,
  infinite: true,
  dots: true,
  nextArrow: '<img src="../images/letters-next.png" alt="" class="slick-next-btn">',
  prevArrow: '<img src="../images/letters-prev.png" alt="" class="slick-prev-btn">',
  responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1,
        infinite: true,
        dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 1,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
})

$(".partners-slider").slick({
  slidesToShow: 7,
  infinite: true,
  nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
  prevArrow: '<img src="../images/arrow-left.png" alt="" class="slick-prev-btn">',
  responsive: [{
      breakpoint: 1024,
      settings: {
        slidesToShow: 5,
        slidesToScroll: 1,
        infinite: true,
        // dots: true
      }
    },
    {
      breakpoint: 600,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    },
    {
      breakpoint: 480,
      settings: {
        slidesToShow: 2,
        slidesToScroll: 1
      }
    }
    // You can unslick at a given breakpoint now by adding:
    // settings: "unslick"
    // instead of a settings object
  ]
})

$(document).ready(function () {

  $('.solution-image').magnificPopup({
    delegate: 'a',
    type: 'image',
  });
  $('.certificate-image').magnificPopup({
    type: 'image',
  });
  $('.about-slide-image').magnificPopup({
    type: 'image',
  });
  $('.letters-zoom').magnificPopup({
    type: 'image'
  });
});

ymaps.ready(init);

function init() {
  var myMap = new ymaps.Map("map", {
    center: [43.26141848, 76.94492184],
    zoom: 11
  }, {
    searchControlProvider: 'yandex#search'
  })
  var myPlacemark = new ymaps.Placemark(
    [43.26141848, 76.94492184]
  );
  myMap.geoObjects.add(myPlacemark);
}

$('.burger-menu-btn').click(function (e) {
  e.preventDefault();
  $(this).toggleClass('burger-menu-lines-active');
  $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
  $('body').toggleClass('body-overflow');
});


$('.about-slider-for').slick({
  slidesToShow: 1,
  slidesToScroll: 1,
  fade: true,
  asNavFor: '.about-slider-nav',
  nextArrow: '<img src="../images/about-arrow2.png" alt="" class="slick-next-about">',
  prevArrow: '<img src="../images/about-arrow.png" alt="" class="slick-prev-about">',
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      infinite: true,
      // dots: true
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
});
$('.about-slider-nav').slick({
  slidesToShow: 5,
  slidesToScroll: 1,
  asNavFor: '.about-slider-for',
  focusOnSelect: true,
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 5,
      slidesToScroll: 1,
      infinite: true,
      // dots: true
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
});

// $('.system-sliders').slick({
//   centerMode: true,
//   centerPadding: 0,
//   slidesToShow: 3,
//   nextArrow: '<img src="../images/about-arrow2.png" alt="" class="slick-next-about">',
//   prevArrow: '<img src="../images/about-arrow.png" alt="" class="slick-prev-about">',
//   responsive: [
//     {
//       breakpoint: 768,
//       settings: {
//         arrows: false,
//         centerMode: true,
//         centerPadding: 0,
//         slidesToShow: 3
//       }
//     },
//     {
//       breakpoint: 480,
//       settings: {
//         arrows: false,
//         centerMode: true,
//         centerPadding: '40px',
//         slidesToShow: 1
//       }
//     }
//   ]
// });

// var swiper = new Swiper('.swiper-container', {
//   effect: 'coverflow',
//   // grabCursor: true,
//   onlyExternal: true,
//   centeredSlides: true,
//   slidesPerView: '3',
//   coverflowEffect: {
//     rotate: 50,
//     // stretch: 0,
//     // depth: 50,
//     // modifier: 3,
//     // slideShadows : true,
//   }
// });



$('.system-slider').slick({
  arrows: true,
  centerMode: true,
  centerPadding: '0px',
  infinite: true,
  swipe: false,
  slidesToShow: 3,
  slidesToScroll:1,
  nextArrow: '<img src="../images/arrow-next.png" alt="" class="slick-next-btn">',
  prevArrow: '<img src="../images/arrow-left.png" alt="" class="slick-prev-btn">',
  responsive: [{
    breakpoint: 1024,
    settings: {
      slidesToShow: 3,
      slidesToScroll: 1,
      infinite: true,
      // dots: true
    }
  },
  {
    breakpoint: 600,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: false,
    }
  },
  {
    breakpoint: 480,
    settings: {
      slidesToShow: 1,
      slidesToScroll: 1,
      centerMode: false,
    }
  }
  // You can unslick at a given breakpoint now by adding:
  // settings: "unslick"
  // instead of a settings object
]
});
